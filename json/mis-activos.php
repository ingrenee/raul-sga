<?php

$fila['id'] = 1;
$fila['edita'] = '<a href="#" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';

$fila['codigo'] = 'CD0121546454';

$fila['codigo2'] = '0000125';

$fila['nombre'] = 'Juan Perez';
$fila['marca'] = 'Honda';
$fila['modelo'] = 'Commit';

$fila['fecha'] = '12/12/2016';
$fila['situacion'] = 'Alta';
$fila['ceco'] = 'Costos';

/*
{
"id":"#",
edita:"",
codigo:"Codigo de inventario",
codigo2:"Codigo del activo",
nombre:"Nombre",
marca:"Marca",
modelo:"Modelo",
fecha:"Fecha de asignacion",
situacion:"Alta",
ceco:"CeCo"
}

 */

$data[] = $fila;
$data[] = $fila;
$data[] = $fila;
$data[] = $fila;
$data[] = $fila;
$data[] = $fila;
$data[] = $fila;
$data[] = $fila;

$r['total'] = 150;
$r['rows'] = $data;

echo json_encode($r);