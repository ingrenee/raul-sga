<?php

$fila['id'] = 1;
$fila['delegado'] = "Maria Fernandez";

$fila['desde'] = '01/01/2016';

$fila['hasta'] = '05/01/2016';

$fila['estado'] = 'Asignado';
$fila['comentarios'] = 'Comentarios extras';
$fila['accion'] = '<a href="#" class="btn btn-block btn-xs btn-danger"> Revocar </a>';

/*

{"id":"#",
delegado:"Delegado a",
desde:"Desde",
hasta:"Hasta",
estado:"Estado",
comentarios:"Comentarios",
accion:""}

 */

$data[] = $fila;
$data[] = $fila;
$data[] = $fila;
$data[] = $fila;
$data[] = $fila;
$data[] = $fila;
$data[] = $fila;
$data[] = $fila;

$r['total'] = 150;
$r['rows'] = $data;

echo json_encode($r);